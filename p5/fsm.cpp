/*
 * CSc103 Project 5: Syntax highlighting, part one.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 
 */

#include "fsm.h"
using namespace cppfsm;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

// make sure this function returns the old state.  See the header
// file for details.
int cppfsm::updateState(int& state, char c) {
	// TODO:  write this function.
	return 0;
}

void testFSM(string s) {
	vector<int> stlist; // list of states.
	int cstate = start;
	for (unsigned long i = 0; i < s.length(); i++) {
		stlist.push_back(updateState(cstate,s[i]));
	}
	// push the last state:
	stlist.push_back(cstate);
	cout << s << endl;
	for (unsigned long i = 0; i < stlist.size(); i++) {
		cout << stlist[i];
	}
	cout << endl;
}

int altMain() {
	// TODO: put your own test code here if you want...
	return 0;
}

int main() {
	// to run your own tests, uncomment the following line:
	// return altMain();

	// read one line at a time, and feed it through
	// the finite state machine:
	string input;
	while(getline(cin,input)) {
		cout << " ";
		testFSM(input);
	}
	return 0;
}
